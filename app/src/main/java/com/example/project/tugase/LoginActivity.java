package com.example.project.tugase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity {
    Button masuk;
    Button Daftar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    masuk =(Button)findViewById(R.id.email_sign_in_button);
    Daftar=(Button)findViewById(R.id.email_daftar_button);

    masuk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
        }
    });
    Daftar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this,DaftarActivity.class);
            startActivity(intent);
        }
    });
    }

}
