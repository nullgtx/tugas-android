package com.example.project.tugase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Produk extends AppCompatActivity {
    Button tambahkeranjang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk);
        tambahkeranjang = (Button)findViewById(R.id.product_cart_btn);
        tambahkeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Produk.this,Keranjang.class);
                startActivity(intent);
            }
        });
    }
}
