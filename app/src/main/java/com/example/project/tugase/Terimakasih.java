package com.example.project.tugase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Terimakasih extends AppCompatActivity {
    Button detail;
    Button beranda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terimakasih);
        detail= (Button)findViewById(R.id.order_status_btn);
        beranda=(Button)findViewById(R.id.continue_shopping_btn);
        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Terimakasih.this,Pemesanan.class);
                startActivity(intent);
            }
        });
        beranda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Terimakasih.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
