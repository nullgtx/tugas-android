package com.example.project.tugase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Pembayaran2 extends AppCompatActivity {
    Button terimakasih;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran2);
        terimakasih= (Button)findViewById(R.id.buttonterimakasih);
        terimakasih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pembayaran2.this,Terimakasih.class);
                startActivity(intent);
            }
        });
    }
}
